# N-body Simulation #

A command-line MPI application that can perform the N-body simulation in parallel on a distributed-memory system.

### Compilation ###

mpicc <file_name>.c -o <file_name> -lm

mpiexec -n 2 ./<file_name>
(2 - number of processes)