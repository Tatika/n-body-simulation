#include <stdio.h>
#include <tgmath.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <mpi.h>
int number_of_planets = 2;

typedef struct {
    float x, y;
} vector2;

const vector2 vector2_zero = { .x = 0, .y = 0 };
const float DT = 0.1;

vector2 add(vector2* v1, vector2* v2) {
    return (vector2) {
        .x = v1->x + v2->x,
        .y = v1->y + v2->y
    };
}

vector2 subtract(vector2* v1, vector2* v2) {
    return (vector2) {
        .x = v1->x - v2->x,
        .y = v1->y - v2->y
    };
}

vector2 multiply(vector2* v, float k) {
    return (vector2) {
        .x = v->x * k,
        .y = v->y * k
    };
}

float sqrMagnitude(vector2* v) {
    return sqrt(pow(v->x, 2) + pow(v->y, 2));
}


const float simulationSofteningLengthSquared = 100.0f * 100.0f;

typedef struct {
    vector2 position;
    vector2 acceleration;
    vector2 velocity;
    float mass;
} Planet;

Planet *planets;

void GenerateDebugData(void);

vector2 CalculateNewtonGravityAcceleration(Planet* first_body, Planet* second_body);

void integrate_bodies(void);

void integrate(Planet *body);
void SimulateWithBruteforce(int *argc, char ***argv, int items_per_process, int number_of_planets0, float DT, float simulationTime);

float delta_time = 0.1;

void GenerateDebugData(void) {

    srand(time(NULL));

    for (int i = 0; i < number_of_planets; ++i) {
        float angle = ((float) i / number_of_planets) * 2.0f * M_PI +
        ((rand() % 100) / 100.0 - 0.5f) * 0.5f;

        Planet* body = &planets[i];

        body->position = (vector2) {
            .x = (rand() % 100) / 100.0,
            .y = (rand() % 100) / 100.0
        };

        body->velocity = (vector2) {
            .x = cos(angle),
            .y = cos(angle)
        };

        float initialMass = 100000.0f;
        body->mass = initialMass * ((rand() % 100) / 100.0) + initialMass * 0.5f;
    }
}

void SimulateWithBruteforce(int *argc, char ***argv, int items_per_process, int number_of_planets0, float DT, float simulationTime) {
    MPI_Init(argc, argv);

    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (rank == 0) {
        printf("%d\n%f\n", number_of_planets0, DT);
        for(int j = 0; j < number_of_planets0; j++) {
            printf("%f %f\n", j + 1, planets[j].position.x, planets[j].position.x);
            printf("%f %f\n", planets[j].acceleration.x, planets[j].acceleration.y);
            printf("%f %f\n", planets[j].velocity.x, planets[j].velocity.y);
            printf("%f\n", j + 1, planets[j].mass);
        }
    }
    MPI_Datatype planetType;

    MPI_Type_contiguous(7, MPI_FLOAT, &planetType);

    MPI_Type_commit(&planetType);
    Planet *local_numbers = (Planet *) malloc(sizeof(*local_numbers) * items_per_process);
    MPI_Scatter(planets, items_per_process, planetType, local_numbers, items_per_process, planetType, 0, MPI_COMM_WORLD);

    for(double time = 0; time < 1; time += DT) {
        for(int i = 0; i < items_per_process; i++) {
                float total_ax = 0.0, total_ay = 0.0;
                for (size_t j = 0; j < number_of_planets0; j++) {
                        if (i + rank * items_per_process == j) {continue;}
                        vector2 temp = CalculateNewtonGravityAcceleration(&local_numbers[i], &planets[j]);
                        total_ax += temp.x;
                        total_ay += temp.y;
                }
                local_numbers[i].acceleration.x = total_ax;
                local_numbers[i].acceleration.y = total_ay;
                printf("%f %f\n", local_numbers[i].acceleration.x, local_numbers[i].acceleration.y);
				

                integrate_bodies();
        }
    }

    Planet *planetsTemp = (Planet*) malloc(number_of_planets * sizeof(*planetsTemp));
    MPI_Gather(local_numbers, items_per_process, planetType, planetsTemp, items_per_process, planetType, 0, MPI_COMM_WORLD);

    free(planets);
    free(local_numbers);

    free(planetsTemp);
    MPI_Finalize();
}
vector2 CalculateNewtonGravityAcceleration(Planet* first_body, Planet* second_body) {
    vector2 acceleration,
            galacticPlaneR,
            galacticPlaneRTimesScale;
    float distanceSquared,
          distanceSquaredCubed,
          inverse,
          scale;

    acceleration = vector2_zero;

    galacticPlaneR = subtract(&first_body->position, &second_body->position);

    distanceSquared = sqrMagnitude(&galacticPlaneR) + simulationSofteningLengthSquared;

    distanceSquaredCubed = pow(distanceSquared, 3);

    inverse = 1.0f / sqrt(distanceSquaredCubed);

    scale = second_body->mass * inverse;

    galacticPlaneRTimesScale = multiply(&galacticPlaneR, scale);

    acceleration = add(&acceleration, &galacticPlaneRTimesScale);

    return acceleration;
}

void integrate_bodies(void) {
    for (int i = 0; i < number_of_planets; ++i) {
        integrate(&planets[i]);
    }
}
void integrate(Planet *body) {
    vector2 acceleration_times_delta_time =
        multiply(&body->acceleration, delta_time);

    body->velocity = add(&body->velocity, &acceleration_times_delta_time);

    vector2 velocity_times_delta_time =
        multiply(&body->velocity, delta_time);
    body->position = add(&body->position, &velocity_times_delta_time);
}
int main(int argc, char** argv) {

    int items_per_process = 10;
    float simulationTime = 1.0;
    planets = (Planet*) malloc(number_of_planets * sizeof(*planets));
    GenerateDebugData ();
    SimulateWithBruteforce(&argc, &argv, items_per_process, number_of_planets, DT, simulationTime);

    return 0;
}


